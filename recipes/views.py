from django.shortcuts import render, get_object_or_404, redirect
from recipes.forms import RecipeForm
from recipes.models import Recipe
from django.contrib.auth.decorators import login_required

# Create your views here.
def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": recipe,
    }
    return render(request, "recipes/detail.html", context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render (request, "recipes/list.html", context)

@login_required
def create_recipe(request):
    if request.method == "POST":
# we should use the form to validate the values
# and save them to the database
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()
# if all goes well, we can redirect the browser
# to another page and leave the function
            return redirect("recipe_list")
    else:
# create an instance of the Django model form class
        form = RecipeForm()
    context = {
        "form": form,
    }
# put the form in the context
    return render(request, "recipes/create.html", context)
# render the HTML template with the form

def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            # redirect back to the page that shows the post
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(instance=recipe)
    context = {
        "recipe": recipe,
        "form": form,
    }
    return render(request, "recipes/edit.html", context)
    
@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)
